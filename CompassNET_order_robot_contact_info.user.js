// ==UserScript==
// @name         CompassNET Order Robot Contact
// @namespace    https://compass-usa.com
// @version      2.1.5
// @updateURL    https://gist.github.com/WisdomWolf/75f7b65c392f9c77521b31cfa1a517a1/raw/CompassNET_order_robot_contact_info.user.js
// @description  Automate CompassNET Order Contact Info
// @author       WisdomWolf
// @match        https://remedyweb.compass-usa.com/arsys/forms/remedyprod/OM%3ADataContacts/Support/*
// @require      https://craig.global.ssl.fastly.net/js/mousetrap/mousetrap.min.js
// @grant        none
// ==/UserScript==
/* jshint -W097 */
'use strict';

var contactName = $('#arid_WIN_0_777900200');
var contactRole = $('#arid_WIN_0_777900201');
var contactPhone = $('#arid_WIN_0_777900202');
var contactPhoneExt = $('#arid_WIN_0_777900209');
var contactPhone2 = $('#arid_WIN_0_777900203');
var contactPhone2Ext = $('#arid_WIN_0_777900210');
var contactEmail = $('#arid_WIN_0_777900204');
var includeYes = $('#WIN_0_rc0id777900013');

var saveButton = $('#WIN_0_777900208');
var unitData;
var runCount;

function updateField(element, value) {
    element.val(value);
    triggerChange(element);
}

function triggerChange(element) {
    if (element.length != undefined) {
        element = element[0];
    }
    if ("createEvent" in document) {
        var evt = document.createEvent("HTMLEvents");
        evt.initEvent("change", false, true);
        element.dispatchEvent(evt);
    }
    else {
        element.fireEvent("onchange");
    }
}

function getUnit() {
    var unitNumber = localStorage.getItem('compass_unit_number');
	console.log('Unit Number: ' + unitNumber);
    if (!unitNumber) {
        var unitNumber = prompt('Unit Number:');
    }
	unitData = $.getJSON('https://tpa.compassfss.com/get_json/' + unitNumber);
    setTimeout(completeForm, 1000);
}

function completeForm() {
    console.log('completing form');
    unitData = $.parseJSON(unitData.responseText);
    
    runCount = localStorage.getItem('contactRunCount');
    console.log('Run Count ' + runCount);
	
	if (runCount == 1) {
        console.log('populating tpa data');
        if (localStorage.getItem('tpa_name' === undefined)) {
            alert('Unable to populate TPA data.  Please set in localStorage and try again');
            return false;
        }
		var name = localStorage.getItem('tpa_name');
		var phone = localStorage.getItem('tpa_phone');
		var email = localStorage.getItem('tpa_email');
	} else if (runCount < 1) {
        console.log('filling unit contact info');
		var name = unitData.unit_contact.name;
		var phone = unitData.unit_contact.phone;
		var email = unitData.unit_contact.email;
	} else {
        return
    }
    
    updateField(contactName, name);
    updateField(contactPhone, phone);
    updateField(contactEmail, email);
    //includeYes.click();
    runCount++;
    setTimeout(saveForm, 500);
}

function saveForm() {
    localStorage.setItem('contactRunCount', runCount);
    saveButton.children().click();
}

setTimeout(function() {
    contactName.addClass('mousetrap');
    contactRole.addClass('mousetrap');
    getUnit();
}, 500);

Mousetrap.bind('1', function(e) {
    getUnit();
    return false;
});

Mousetrap.bind('2', function(e) {
    completeForm();
    return false;
});

Mousetrap.bind('ctrl+s', function(e) {
    saveform();
    return false;
});